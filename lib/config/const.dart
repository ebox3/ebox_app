import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xffFE7655);
  static const Color backgroundColor = Color(0xffFFFFFF);
  static const Color secondaryBackgroundColor = Color(0xffF8F8F8);
  static const Color iconColor = Color(0xffB8C2C8);
  static const Color textColor = Color(0xff6b7280);
  static const Color lightBlue = Color(0xFFe3ecfa);
}
