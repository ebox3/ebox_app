import 'package:ebox/models/Ingredient.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MealPlan {
  String? id;
  String? name;
  String? imageUrl;
  String? price;

  MealPlan({this.id, this.name, this.imageUrl, this.price});

  MealPlan.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    imageUrl = json['imageUrl'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['imageUrl'] = this.imageUrl;
    data['price'] = this.price;
    return data;
  }

  MealPlan.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    id = documentSnapshot.id;
    name = documentSnapshot.get('name');
    price = documentSnapshot.get('price');
    imageUrl = documentSnapshot.get('imageUrl');
  }
}
