import 'package:ebox/models/Ingredient.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Recipe {
  String? id;
  String? name;
  String? imageUrl;
  List<Ingredient>? ingredients;
  int? price;

  Recipe({this.id, this.name, this.imageUrl, this.ingredients, this.price});

  Recipe.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    imageUrl = json['imageUrl'];
    if (json['ingredients'] != null) {
      ingredients = <Ingredient>[];
      json['ingredients'].forEach((v) {
        ingredients!.add(new Ingredient.fromJson(v));
      });
    }
    price = json['price'];
  }

  get length => null;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['imageUrl'] = imageUrl;
    if (ingredients != null) {
      data['ingredients'] = ingredients!.map((v) => v.toJson()).toList();
    }
    data['price'] = price;
    return data;
  }

  Recipe.fromDocumentSnapshot(DocumentSnapshot documentSnapshot) {
    id = documentSnapshot.id;
    name = documentSnapshot.get('name');
    price = documentSnapshot.get('price');
    ingredients = List<Ingredient>.from(
      documentSnapshot["ingredients"].map(
        (ingredientSnap) {
          {
            return Ingredient(
              id: ingredientSnap["id"],
              category: ingredientSnap["category"],
              imageUrl: ingredientSnap["imageUrl"],
              name: ingredientSnap["name"],
              price: ingredientSnap["price"],
              quantity: ingredientSnap["quantity"],
            );
          }
        },
      ),
    );
    imageUrl = documentSnapshot.get('imageUrl');
  }
}
