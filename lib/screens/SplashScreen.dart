import 'dart:async';

import 'package:ebox/screens/AuthScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // bool firstBoarding;

  _navigateAfterSplash() {
    Timer(
      Duration(milliseconds: 2000),
      () => Get.off(() => AuthScreen(), transition: Transition.fadeIn),
    );
  }

  @override
  void initState() {
    super.initState();
    _navigateAfterSplash();
  }

  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          colors: [Color(0XFFF4822D), Color(0XFFFE7655)],
        ),
      ),
      child: Center(
        child: SvgPicture.asset(
          'assets/logo/icon-white.svg',
          color: Colors.white,
          height: 120,
        ),
      ),
    );
  }
}
