
import 'package:ebox/models/Recipe.dart';
import 'package:flutter/material.dart';

class RecipeList extends StatelessWidget {
  final Recipe content;
  const RecipeList({Key? key, required this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            Text(
              content.name.toString(),
            ),
            Container(
              width: double.infinity,
              child: DataTable(columns: [
                DataColumn(
                  label: Text('Ingredient'),
                ),
              ], rows: []),
            )
          ],
        ));
  }
}
