
import 'package:ebox/config/const.dart';
import 'package:ebox/screens/MealPlanList/MealPlanList.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // final _mealPlanController = Get.put(MealPlanController());

  // Future<List<MealPlan>>? getMealPlanList;
  // List<MealPlan>? retrievedMealPlanList;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _mealPlanController.queryMealPlan();
    // print(_mealPlanController.mealPlans[0]);
    // _initRetrieval();
    // getMealPlanList = _mealPlanController.queryMealPlan();

    // print(getMealPlanList);
  }

  // Future<void> _initRetrieval() async {
  //   getMealPlanList = _mealPlanController.queryMealPlan();
  //   retrievedMealPlanList = await _mealPlanController.queryMealPlan();

  //   print(retrievedMealPlanList);
  // }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.secondaryBackgroundColor,
      appBar: AppBar(
        backgroundColor: AppColors.backgroundColor,
        brightness: Brightness.light,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.menu,
            color: AppColors.textColor,
          ),
          onPressed: () {},
        ),
      ),
      body: MealPlanList(),
    );
  }
}

// SafeArea(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             Container(
//               width: double.infinity,
//               decoration: BoxDecoration(
//                 color: AppColors.backgroundColor,
//                 borderRadius: BorderRadius.vertical(
//                   bottom: Radius.circular(20),
//                 ),
//               ),
//               padding: const EdgeInsets.all(20),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     "You are",
//                     style: TextStyle(
//                       fontSize: 25,
//                     ),
//                   ),
//                   SizedBox(
//                     height: 3,
//                   ),
//                   Text(
//                     "what you eat!",
//                     style: TextStyle(
//                       color: AppColors.primaryColor,
//                       fontSize: 40,
//                     ),
//                   )
//                 ],
//               ),
//             ),
//             SizedBox(
//               height: 20,
//             ),
//             // MealPlans(),
//             // Container(
//             //   width: double.infinity,
//             //   height: 250,
//             //   child: FutureBuilder(
//             //     future: getMealPlanList,
//             //     builder: (BuildContext context,
//             //         AsyncSnapshot<List<MealPlan>> snapshot) {
//             //       if (snapshot.hasData && snapshot.data!.isNotEmpty) {
//             //         return ListView.separated(
//             //             itemCount: retrievedMealPlanList!.length,
//             //             separatorBuilder: (context, index) => const SizedBox(
//             //                   height: 10,
//             //                 ),
//             //             itemBuilder: (context, index) {
//             //               return Container(
//             //                 decoration: BoxDecoration(
//             //                   image: DecorationImage(
//             //                     image: NetworkImage(
//             //                       retrievedMealPlanList![index]
//             //                           .imageUrl
//             //                           .toString(),
//             //                     ),
//             //                     fit: BoxFit.cover,
//             //                   ),
//             //                 ),
//             //                 child: ListTile(
//             //                   onTap: () {
//             //                     // Navigator.pushNamed(context, "/edit", arguments: retrievedEmployeeList![index]);
//             //                   },
//             //                   shape: RoundedRectangleBorder(
//             //                     borderRadius: BorderRadius.circular(8.0),
//             //                   ),
//             //                   title: Text(retrievedMealPlanList![index]
//             //                       .name
//             //                       .toString()),
//             //                   subtitle: Text(
//             //                       "${retrievedMealPlanList![index].price.toString()}"),
//             //                   trailing: const Icon(Icons.arrow_right_sharp),
//             //                 ),
//             //               );
//             //             });
//             //       }
//             //       return Container(
//             //         child: Text("test failed"),
//             //       );
//             //     },
//             //   ),
//             // )
//             // GridView.count(
//             //   primary: false,
//             //   crossAxisCount: 2,
//             //   children: [
//             //     Container(
//             //       padding: const EdgeInsets.all(8),
//             //       color: Colors.teal[100],
//             //       child: const Text("He'd have you all unravel at the"),
//             //     ),
//             //     Container(
//             //       padding: const EdgeInsets.all(8),
//             //       color: Colors.teal[200],
//             //       child: const Text('Heed not the rabble'),
//             //     ),
//             //     Container(
//             //       padding: const EdgeInsets.all(8),
//             //       color: Colors.teal[300],
//             //       child: const Text('Sound of screams but the'),
//             //     ),
//             //   ],
//             // )
//             // const MealPlanCard(
//             //   name: "Okay",
//             //   price: "10",
//             // ),
//             // const MealPlanCard(
//             //   name: "Okay",
//             //   price: "10",
//             // ),
//           ],
//         ),
//       ),
