import 'package:ebox/controllers/IngredientController.dart';
import 'package:ebox/controllers/MealPlanController.dart';
import 'package:ebox/controllers/RecipeController.dart';
import 'package:ebox/models/Ingredient.dart';
import 'package:ebox/models/MealPlan.dart';
import 'package:ebox/models/Recipe.dart';
import 'package:ebox/screens/MealPlanList/widgets/MealPlanSkeleton.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../config/const.dart';
import '../../controllers/HomeController.dart';

import 'widgets/MealPlanCard.dart';

class MealPlanList extends StatefulWidget {
  const MealPlanList({Key? key}) : super(key: key);

  @override
  State<MealPlanList> createState() => _MealPlanListState();
}

class _MealPlanListState extends State<MealPlanList> {
  final _mealPlanController = Get.put(MealPlanController());
  final _ingredientCardController = Get.put(IngredientController());
  final _recipeController = Get.put(RecipeController());

  var currentCategory;

  var listOfCategory = ["meat", "vegetable", "herb"];

  var currentIndex = 0;

  var ingredients = <Ingredient>[].obs;

  @override
  void initState() {
    // TODO: implement initState
    _mealPlanController.queryMealPlans();
    _ingredientCardController.queryIngredients(listOfCategory[currentIndex]);
    _recipeController.queryRecipes();
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      body: SafeArea(
          child: Column(
        children: [
          Expanded(
            child: Obx(
              () => _mealPlanController.loading.value
                  ? ListView.builder(
                      itemCount: 3,
                      itemBuilder: (BuildContext context, int index) {
                        return const MealPlanSkeleton();
                      },
                    )
                  : ListView.builder(
                      itemCount: _mealPlanController.listOfMealPlan.length,
                      itemBuilder: (BuildContext context, int index) {
                        return MealPlanCard(
                          content: _mealPlanController.listOfMealPlan[index],
                        );
                      },
                    ),
            ),
          ),
          Container(
            height: 50.0,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: listOfCategory.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: (() {
                    setState(() {
                      currentIndex = index;
                      _ingredientCardController
                          .queryIngredients(listOfCategory[currentIndex]);
                    });
                  }),
                  child: Text("${listOfCategory[index]}"),
                );
              },
            ),
          ),
          Expanded(
            child: Obx(
              () => _ingredientCardController.loading.value
                  ? ListView.builder(
                      itemCount: 2,
                      itemBuilder: (BuildContext context, int index) {
                        return const MealPlanSkeleton();
                      },
                    )
                  : ListView.builder(
                      itemCount: _ingredientCardController.ingredients.length,
                      itemBuilder: (BuildContext context, int index) {
                        return IngredientCard(
                          content: _ingredientCardController.ingredients[index],
                        );
                        // return Container();
                      },
                    ),
            ),
          ),
          Expanded(
            child: Obx(
              () => _ingredientCardController.loading.value
                  ? ListView.builder(
                      itemCount: 2,
                      itemBuilder: (BuildContext context, int index) {
                        return const MealPlanSkeleton();
                      },
                    )
                  : ListView.builder(
                      itemCount: _recipeController.listOfRecipes.length,
                      itemBuilder: (BuildContext context, int index) {
                        return RecipeCard(
                          content: _recipeController.listOfRecipes[index],
                        );
                        // return Container();
                      },
                    ),
            ),
          ),
        ],
      )),
    );
  }
}

class RecipeCard extends StatelessWidget {
  final Recipe content;
  const RecipeCard({
    Key? key,
    required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: GestureDetector(
        onTap: () {
          // await FirebaseAuth.instance.signOut();
          // print(content.ingredients.frow);
          content.ingredients?.forEach((Ingredient e) {
            print(e.toJson());
          });
        },
        child: Container(
          width: double.infinity,
          height: 150,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(
                content.imageUrl.toString(),
              ),
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                begin: Alignment.bottomRight,
                stops: const [0.2, 0.9],
                colors: [
                  Colors.black.withOpacity(.8),
                  Colors.black.withOpacity(.1),
                ],
              ),
            ),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text(
                  content.name.toString(),
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class MealPlanCard extends StatelessWidget {
  final MealPlan content;
  const MealPlanCard({
    Key? key,
    required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: GestureDetector(
        onTap: () {
          // await FirebaseAuth.instance.signOut();
          print(content.id);
        },
        child: Container(
          width: double.infinity,
          height: 150,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(
                content.imageUrl.toString(),
              ),
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                begin: Alignment.bottomRight,
                stops: const [0.2, 0.9],
                colors: [
                  Colors.black.withOpacity(.8),
                  Colors.black.withOpacity(.1),
                ],
              ),
            ),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text(
                  content.name.toString(),
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class IngredientCard extends StatelessWidget {
  final Ingredient content;
  const IngredientCard({
    Key? key,
    required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: GestureDetector(
        onTap: () {
          // await FirebaseAuth.instance.signOut();
          print(content.id);
        },
        child: Container(
          width: double.infinity,
          height: 150,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image: DecorationImage(
              fit: BoxFit.cover,
              image: NetworkImage(
                content.imageUrl.toString(),
              ),
            ),
          ),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              gradient: LinearGradient(
                begin: Alignment.bottomRight,
                stops: const [0.2, 0.9],
                colors: [
                  Colors.black.withOpacity(.8),
                  Colors.black.withOpacity(.1),
                ],
              ),
            ),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: const EdgeInsets.all(15),
                child: Text(
                  content.name.toString(),
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
