import 'package:ebox/widgets/SkeletonLoading.dart';
import 'package:flutter/cupertino.dart';

class MealPlanSkeleton extends StatelessWidget {
  const MealPlanSkeleton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const [
          SkeletonLoading.rectangular(
            height: 150.0,
          ),
          SkeletonLoading.rectangular(
            width: 300.0,
            height: 16.0,
          ),
          SkeletonLoading.rectangular(
            width: 150.0,
            height: 14.0,
          ),
        ],
      ),
    );
  }
}
