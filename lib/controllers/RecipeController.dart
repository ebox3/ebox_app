import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ebox/models/Recipe.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

class RecipeController extends GetxController {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseStorage _storage = FirebaseStorage.instance;

  final listOfRecipes = <Recipe>[].obs;
  var loading = false.obs;

  @override
  onInit() async {
    await fetchRecipeLists();
    super.onInit();
  }

  fetchRecipeLists() async {
    loading.value = true;
    final recipeList = await queryRecipes();
    listOfRecipes.value = recipeList;

    loading.value = false;
  }

  Future<List<Recipe>> queryRecipes() async {
    QuerySnapshot recipesSnapshot =
        await _firestore.collection("recipes").get();

    List<Recipe> recipeList = [];

    recipesSnapshot.docs.forEach((q) {
      // print("dog");
      // print(MealPlan.fromDocumentSnapshot(q));
      recipeList.add(new Recipe.fromDocumentSnapshot(q));
    });

    return recipeList;
  }
}
