import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ebox/models/MealPlan.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

class MealPlanController extends GetxController {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseStorage _storage = FirebaseStorage.instance;

  final listOfMealPlan = <MealPlan>[].obs;
  var loading = false.obs;

  @override
  onInit() async {
    await fetchMealPlanLists();
    super.onInit();
  }

  fetchMealPlanLists() async {
    loading.value = true;
    final mealPlanList = await queryMealPlans();
    listOfMealPlan.value = mealPlanList;

    loading.value = false;
  }

  Future<List<MealPlan>> queryMealPlans() async {
    QuerySnapshot mealPlanSnapshot =
        await _firestore.collection("meal_plan").get();

    List<MealPlan> mealPlanList = [];

    mealPlanSnapshot.docs.forEach((q) {
      // print("dog");
      // print(MealPlan.fromDocumentSnapshot(q));
      mealPlanList.add(new MealPlan.fromDocumentSnapshot(q));
    });

    return mealPlanList;
  }
}
