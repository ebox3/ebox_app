
import 'package:ebox/controllers/RecipeController.dart';
import 'package:ebox/models/Recipe.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  var recipesList = <Recipe>[].obs;
  var loading = false.obs;

  @override
  onInit() async {
    await fetchHomeContents();
    super.onInit();
  }

  fetchHomeContents() async {
    loading.value = true;
    final _recipeController = Get.put(RecipeController());
    final recipeList = await _recipeController.queryRecipes();
    recipesList.value = recipeList;

    loading.value = false;
  }
}
