import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:ebox/models/Ingredient.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:get/get.dart';

class IngredientController extends GetxController {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseStorage _storage = FirebaseStorage.instance;

  final ingredients = <Ingredient>[].obs;
  var loading = false.obs;

  @override
  onInit() async {
    // await fetchMealPlanLists();
    super.onInit();
  }

  // fetchProductLists() async {
  //   loading.value = true;
  //   final productList = await queryProducts();
  //   listOfProducts.value = productList;

  //   loading.value = false;
  // }

  queryIngredients(String category) async {
    loading.value = true;
    QuerySnapshot ingredientSnapshot = await _firestore
        .collection("products")
        .where("category", isEqualTo: category)
        .get();

    List<Ingredient> queryIngredients = [];

    ingredientSnapshot.docs.forEach((q) {
      // print("dog");
      // print(MealPlan.fromDocumentSnapshot(q));
      queryIngredients.add(new Ingredient.fromDocumentSnapshot(q));
    });

    ingredients.value = queryIngredients;
    loading.value = false;
  }
}
