import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class PrefKey {
  static const USER_INFO = "user_info";
  static const USER_TOKEN = "user_token";
  static const USER_PROFILE = "user_profile";
  static const USER_INFORMATION = "user_information";
  // static const USER_OTP_TOKEN = "user_otp_token";
  static const FIRST_BOARDING = "first_boarding";
  // static const RESET_PASSWORD_OTP_TOKEN = "reset_password_otp_token";
  // static const NOTIFICATION_LIST = "notification_list";
  // static const UNREAD_NOTIFICATION_COUNT = "unread_notification_count";
  static const LANGUAGE = "en";
  // static const CAMERA = "-1";
}

class SharedPrefProvider {
  read(String key) async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getString(key) == null) {
      return null;
    }
    return json.decode(prefs.getString(key)!);
  }

  save(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));
  }

  readBoolean(String key) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  saveBoolean(String key, value) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(key, value);
  }

  remove(String key) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
  }
}
