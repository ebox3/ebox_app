import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shimmer/shimmer.dart';

import '../config/const.dart';

class SkeletonLoading extends StatelessWidget {
  final double width;
  final double height;

  const SkeletonLoading.rectangular({
    this.width = double.infinity,
    required this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 5.0),
      child: Shimmer.fromColors(
        baseColor: Color(0xffF2F2F2),
        highlightColor: Color(0xffF8F8F8),
        child: Container(
          width: width,
          height: height,
          color: Colors.grey,
        ),
      ),
    );
  }
}
